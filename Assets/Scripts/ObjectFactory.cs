﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFactory : ScriptableObject {

   

   public virtual  Actor CreateActor(Vector3 position, Quaternion rotation,Actor gObject)
    {
        var newObject = Instantiate(gObject,position,rotation);
        return newObject;
    }

}
