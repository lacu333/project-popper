﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerController))]
public class UI_Player : UI_Controller{

    public Slider healthSlider;
    public Text debugText;



    PlayerController observablePlayer;

    private void Awake()
    {
        observablePlayer = GetComponent<PlayerController>();
    }

    // Use this for initialization
    void Start () {
        observablePlayer.UIValueChange_Health += SetHealth;
        observablePlayer.UIValueChange_DebugText += SetDebugText;
    }

    public void SetHealth()
    {
        healthSlider.value = observablePlayer.GetHealth();
    }

    public void SetDebugText()
    {
        debugText.text = observablePlayer.GetDebugText();
    }

    public void SetObservablePlayer(PlayerController playerController)
    {
        observablePlayer = playerController;
    }

}
