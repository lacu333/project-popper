﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private string debugText;

    public float invulnerabilityTime=3.0f;
    private float health = 1;
    private bool invulnerability = false;
    private bool isAlive = true;

    public PoolableObject PO_Bullet_1;
    public Bullet bulletPrefab;
    private ObjectFactory bulletFactory;
    private PlayerCannon leftCannon, rightCannon;
    private Rigidbody2D rb;

    private float moveThreshold = 0.1f;// Minimum tilt for it to start moving
    public float speedMod = 100.0f;
    private float velocity = 0.0f;      // Current Travelling Velocity
    private float acc = 0.0f;           // Current Acceleration

    private Color bulletColorLeft;
    private Color bulletColorRigth;

    private float leftBulletSize;
    private float rightBulletSize;
    public float minBulletSize = 0.5f;
    public float maxBulletSize = 1.0f;
    public float bulletIncreaseRate = 0.1f;
    private Coroutine chargingLeftBullet = null;
    private Coroutine chargingRightBullet = null;


    private bool tappingLeft = false;
    private bool tappingRight = false;

    //Event Delegates that will notify UI_Player when the values change
    public event Action UIValueChange_Health;
    public event Action UIValueChange_DebugText;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        bulletFactory = ScriptableObject.CreateInstance<ObjectFactory>();//Creates the factory to use when player appears

        leftCannon = ScriptableObject.CreateInstance<PlayerCannon>();
        rightCannon = ScriptableObject.CreateInstance<PlayerCannon>();
        bulletColorLeft = GameManager.instance.color1;
        bulletColorRigth = GameManager.instance.color2;
        leftCannon.init(bulletPrefab, bulletFactory, bulletColorLeft, 1, PO_Bullet_1);
        rightCannon.init(bulletPrefab, bulletFactory, bulletColorRigth, 2, PO_Bullet_1);
        UpdateCannonPosRot();

        leftBulletSize = minBulletSize;
        rightBulletSize = minBulletSize;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("Invulnerability: "+ invulnerability.ToString());
        if (!invulnerability)
        {
            if (collision.gameObject.GetComponent<Enemy>() != null)
            {
                print("Enemy touched");
                ChangeHealth(-GameManager.instance.damageFromEnemies); //Save this in a variable locally if its not gonna change during a game. ***
                invulnerability = true;
               StartCoroutine(InvulnerabilityDuration(invulnerabilityTime));
            }
        }

    }


    // Use this for initialization
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            var x = Input.acceleration.x;//Tilt input

            if (Mathf.Abs(x) > moveThreshold)//Minimum tilt for it to move
            {
                if (Mathf.Sign(x) > 0)//If its positive 
                {
                    acc = x - moveThreshold;
                }
                else//If its negative 
                {
                    acc = x + moveThreshold;
                }
                if (acc == 0)
                {
                    acc = 0.1f;//Minimum speed
                }
            }
            else
            {
                acc = 0;//If tilt is not high enough it wont move
            }
            velocity = acc * speedMod;
            rb.velocity = new Vector2(velocity, 0);//Move character left and right
            UpdateCannonPosRot();//Everytime it moves it will update cannons' positions

            debugText = "LeftBulletSize: " + leftBulletSize + "\nRightBulletSize: " + rightBulletSize;
            UIValueChange_DebugText();

            ChargeBullet();//Check if player is holding button in order to increase size of the respective bullet
        }
    }
    void UpdateCannonPosRot()
    {
        //*********Add check if it has changed, so it doesn't updates all the time***********
        leftCannon.position = transform.position + new Vector3(-0.3f, 0.5f);
        rightCannon.position = transform.position + new Vector3(0.3f, 1);
        leftCannon.rotation = transform.rotation;
        rightCannon.rotation = transform.rotation;

    }

    //Function that increases the size of the bullets
    public void ChargeBullet()
    {
        if (tappingLeft)
        {
            if (chargingLeftBullet == null)
            {
                chargingLeftBullet= StartCoroutine(LeftBulletCharge());
            }
        }
        else
        {
            CleanCoroutine(ref chargingLeftBullet);
        }

        if (tappingRight)
        {
            if (chargingRightBullet == null)
            {
                chargingRightBullet = StartCoroutine(RightBulletCharge());
            }
        }
        else
        {
            CleanCoroutine(ref chargingRightBullet);
        }
    }
    public void CleanCoroutine(ref Coroutine coroutine)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }
    }


    //When player starts holding a button it changes the respective bool to true so the respective bullet grows bigger
    public void ChargingCannon(int cannonID)
    {
        if (isAlive)
        {
            switch (cannonID)
            {
                case 1:
                    if (!tappingLeft)
                    {
                        tappingLeft = true;
                    }
                    break;

                case 2:
                    if (!tappingRight)
                    {
                        tappingRight = true;
                    }
                    break;
            }
        }
    }

    //When player stops holding a button it shoots and frees the bool so another bullet can be used afterwards
    public void UseCannon(int cannonID)
    {
        if (isAlive)
        {
            switch (cannonID)
            {
                case 1://Left cannon
                    leftCannon.CheckInputShoot(leftBulletSize);
                    leftBulletSize = minBulletSize;//Reset the actual bulletSize to minimum size
                  
                    tappingLeft = false;
                    break;

                case 2://Right Cannon
                    rightCannon.CheckInputShoot(rightBulletSize);
                    rightBulletSize = minBulletSize;//Reset the actual bulletSize to minimum size
                   
                    tappingRight = false;
                    break;
            }
        }
    }

    public void ChangeHealth(float value)
    {
        health += value;
        if (health <= 0)
        {
            isAlive = false;
        }
        UIValueChange_Health();//Notify that health changed
    }
    public float GetHealth() {return health;}

    public string GetDebugText() { return debugText; }

    IEnumerator InvulnerabilityDuration(float time)
    {
        yield return new WaitForSeconds(time);//Waits this time to be able to take damage again
        invulnerability = false;
    }


    IEnumerator LeftBulletCharge()
    {
        yield return new WaitForSeconds(0.5f);
        if (tappingLeft)
        {
            leftBulletSize += bulletIncreaseRate;
            if (leftBulletSize > maxBulletSize)//If its at max coroutine will stop cycling
            {
                leftBulletSize = maxBulletSize;
                chargingLeftBullet = null;
            }
            else//If its not at max then keep cycling until it is
            {
                StartCoroutine(LeftBulletCharge());
            }
        }else { chargingLeftBullet = null; }
       
    }
    IEnumerator RightBulletCharge()
    {
        yield return new WaitForSeconds(0.5f);
        if (tappingRight)
        {
            rightBulletSize += bulletIncreaseRate;
            if (rightBulletSize > maxBulletSize)//If its at max coroutine will stop cycling
            {
                rightBulletSize = maxBulletSize;
                chargingRightBullet = null;
            }
            else//If its not at max then keep cycling until it is
            {
                StartCoroutine(RightBulletCharge());
            }
        } else { chargingRightBullet = null; }
    }

}
