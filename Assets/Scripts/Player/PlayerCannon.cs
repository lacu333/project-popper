﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCannon : ScriptableObject {

    //private Bullet bulletPrefab;
    //private ObjectFactory bulletFactory;

    private PoolableObject PO_Bullet_1;

    private Color bulletColor;
    private int colorID;

    public Vector3 position;
    public Quaternion rotation;

    private bool canShoot1 = true;

    bool shootKeyDown = false;
    bool shootKeyUp = true;


    public void init(Bullet bulletPrefab, ObjectFactory bulletFactory, Color bulletColor, int colorID, PoolableObject PO_Bullet_1)//Constructor
    {
        //this.bulletPrefab = bulletPrefab;
        //this.bulletFactory = bulletFactory;

        this.PO_Bullet_1 = PO_Bullet_1;
        this.bulletColor = bulletColor;
        this.colorID = colorID;
    }

    public void CheckInputShoot(float bulletSize)
    {//[up1,up2,down1,down2]    
        if (shootKeyUp)//If its not being pressed
        {
            shootKeyDown = true;
        }
        if (shootKeyDown)//If it is being pressed
        {
            shootKeyUp = true;
            if (shootKeyUp)
            {
                ShootBullet(bulletSize);//Chooses which cannon to use and shoots the respective colored bullet
            }
        }

    }

    void ShootBullet(float bulletSize)
    {//Shooting method

    
        Bullet bulletClone = PoolManager.instance.GetObjectFromPool(PO_Bullet_1) as Bullet;
        bulletClone.SetValues(colorID,bulletColor,position,rotation, bulletSize);
   
        bulletClone.gameObject.SetActive(true);
        canShoot1 = true;//Unlock shooting button
    }

}

