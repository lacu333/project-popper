﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PoolableObject : MonoBehaviour {


    public float lifeSpan;

    protected void OnEnable()
    {
        if (lifeSpan >= 0)
        {
            StartCoroutine(Disabler());
        }
        OnEnable_PO();
    }

    protected void OnDisable()
    {
        StopCoroutine(Disabler());
    }

    IEnumerator Disabler()
    {
        yield return new WaitForSeconds(lifeSpan);
        gameObject.SetActive(false);
        OnDisable_PO();
    }

    protected abstract void OnEnable_PO();
    protected abstract void OnDisable_PO();
}
