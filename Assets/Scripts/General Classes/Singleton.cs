﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>{

    public static T instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = GetComponent<T>();
        }
        else
        {
            Debug.LogWarning("Warning: Theres is already an instance of " + typeof(T) + " in the scene!");
            Destroy(this);
        }


    }



}
