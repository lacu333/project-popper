﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Enemy : PoolableObject{

    private float health=1.0f;
    private float size = 0.1f;
    public float massIncreaseSmall = 100;
    public float massIncreaseMedium = 100;
    private float massIncrease;
    private float resistance=0;

    public float smallSizeRef=0.1f;
    public float mediumSizeRef=0.5f;
    public float maxSizeRef=1;

    public float smallResistanceRef=0;
    public float mediumResistanceRef=0.5f;
    public float maxResistanceRef=0.9f;

    public Enemy prefabToSplitInto; // check if i use it 
    public int smallNumberSplits=0;
    public int mediumNumberSplits = 2;
    public int bigNumberSplits = 2;
    private int currentNumSplits=0;
    private float splitSizes=1;// check if i use it 

    private float speedMod;
     private int colorID;
   


    protected SpriteRenderer spriteR;
    protected Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteR = GetComponent<SpriteRenderer>();
        massIncrease = massIncreaseSmall;
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void SetValues(int colorID, Color color, Vector3 position)
    {
        this.colorID = colorID;
    spriteR.color = color;
        rb.transform.position = position;
        
    }

    protected override void OnDisable_PO()
    {
        
    }

    protected override void OnEnable_PO()
    {
       
    }

    public virtual void StartMoving(float speed)
    {

    }

    //Getters
    public int GetColorID()
    {
        return colorID;
    }
    public float GetSize()
    {
        return size;
    }
    public float GetResistance()
    {
        return resistance;
    }
    public float GetHealth()
    {
        return health;
    }

    //Setters
    public void SetSize(float size)
    {
            this.size = size;
        if (this.size > maxSizeRef)//IF its more than tha max then the max gets assigned to it
        {
            this.size = maxSizeRef;
        }
        else// If its not more than the max then it assigns the rest of the values
        {
            rb.mass = this.size * massIncrease;
            transform.localScale = new Vector3(this.size, this.size, 1);
            AssignValuesWithSize();
        }
            
    }
    public void SetSplitSizes(float splitSizes)
    {
        this.splitSizes = splitSizes;

    }
    public void ChangeHealth(float value)
    {
        health += value;
        if (health <=0)//If it dies
        {
            if (currentNumSplits>0)
            {
             
                AssignSplitSizes();
                Spawner.instance.SplitEnemy(currentNumSplits, prefabToSplitInto, splitSizes, colorID, transform.position);//Split
            }
                gameObject.SetActive(false);
        }
    }

    public void AssignValuesWithSize()
    {
        if (size < mediumSizeRef)//If it is small
        {
            resistance = smallResistanceRef;
            currentNumSplits = smallNumberSplits;
            if (massIncrease != massIncreaseSmall)
            {
                massIncrease = massIncreaseSmall;
            }
        }
        else if (size < maxSizeRef)//If it is medium
        {
            resistance = mediumSizeRef;
            currentNumSplits = mediumNumberSplits;
            if (massIncrease != massIncreaseMedium)
            {
                massIncrease = massIncreaseMedium;
            }
        }
        else//If it is big
        {
            resistance = maxSizeRef;
            currentNumSplits = bigNumberSplits;
        }
    }

    public virtual void AssignSplitSizes() { print("OLD METHOD"); }
}
