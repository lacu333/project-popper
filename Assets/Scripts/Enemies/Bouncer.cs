﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : Enemy {




    public override void StartMoving(float speed)
    {
        Vector3 randomDirection = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        rb.velocity = randomDirection * speed;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void AssignSplitSizes()//Compares its current size to assign a size for the splits
    {
        print("NEW METHOD.   SIZE: "+GetSize());
        if (GetSize()>=mediumSizeRef && GetSize()<maxSizeRef)
        {
            print("Small size set");
            SetSplitSizes(smallSizeRef);
        }
        else if(GetSize()==maxSizeRef)
            {
            print("Medium size set");
            SetSplitSizes(mediumSizeRef);
        }
    }
}
