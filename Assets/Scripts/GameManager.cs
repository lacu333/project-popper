﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    GameSettings gameSettings; // Change this to serialization, so it actually saves settings in the device and not a class during gameplay.

    //Game Time
    int sec;
    int min;

    int enemyCount;

    float score;

   public  Color color1;
    public Color color2;

    public int difficulty= 0;
    private float minEnemySpeed=10;
    private float maxEnemySpeed=20;
    public float damageFromEnemies=0.1f;

    // Use this for initialization
    void Start () {
        //Give the spawner the starting values for the enemies that will be spawned
        Spawner.instance.SetMinEnemySpeed(minEnemySpeed);
        Spawner.instance.SetMaxEnemySpeed(maxEnemySpeed);
        Spawner.instance.SetMinEnemySize(0.1f);
        Spawner.instance.SetMaxEnemySize(0.3f);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
