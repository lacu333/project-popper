﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : Singleton<Spawner>
{

    public PoolableObject prefabToSpawn;
    public int poolSizeEnemies=3;

    public float startingCooldown=3.0f;

    private float minEnemySpeed, maxEnemySpeed = 0.1f;

    private float minEnemySize, maxEnemySize;

    public void Start()
    {
        PoolManager.instance.CreatePool(prefabToSpawn,poolSizeEnemies);
        StartCoroutine(Spawn(startingCooldown));
    }

    IEnumerator Spawn(float cooldown)
    {
        yield return new WaitForSeconds(cooldown);
        Enemy enemyClone = PoolManager.instance.GetObjectFromPool(prefabToSpawn) as Enemy;

        //Generate a random size
        float randomSize = Random.Range(minEnemySize, maxEnemySize);
        enemyClone.SetSize(randomSize);
        //Generate random color number
        Color colorToApply = Color.red;
        int colorID = Random.Range(1, 3);
        switch (colorID)
        {
            case 1:
                colorToApply = GameManager.instance.color1;
                break;
            case 2:
                colorToApply= GameManager.instance.color2;
                break;

        }
        enemyClone.SetValues(colorID, colorToApply,new Vector3(0,0,0));
        enemyClone.gameObject.SetActive(true);
        enemyClone.GetComponent<Enemy>().StartMoving(Random.Range(minEnemySpeed,maxEnemySpeed));
        //*******************************Generate new time to wait**********************************

        StartCoroutine(Spawn(cooldown));
    }

    public void SplitEnemy(int numSplits, PoolableObject prefab, float size, int colorID, Vector3 position)
    {
        for (int i =0; i<numSplits;i++) {
            Enemy enemyClone = PoolManager.instance.GetObjectFromPool(prefab) as Enemy;

            //Assign size
            enemyClone.SetSize(size);

            //Assign color
            Color colorToApply = Color.red;

            switch (colorID)
            {
                case 1:
                    colorToApply = GameManager.instance.color1;
                    break;
                case 2:
                    colorToApply = GameManager.instance.color2;
                    break;
            }
            //Vary position so not all the splits spawn in the same place
            position.x += Random.Range(0,1);
            position.y += Random.Range(0, 1);

            enemyClone.SetValues(colorID, colorToApply, position);
            enemyClone.gameObject.SetActive(true);
            enemyClone.GetComponent<Enemy>().StartMoving(Random.Range(minEnemySpeed, maxEnemySpeed));
        }
    }

    //Setters
    public void SetMinEnemySpeed(float speed)
    {
        minEnemySpeed = speed;
    }
    public void SetMaxEnemySpeed(float speed)
    {
        maxEnemySpeed = speed;
    }

    public void SetMinEnemySize(float minEnemySize)
    {
        this.minEnemySize = minEnemySize;

    }
    public void SetMaxEnemySize(float maxEnemySize)
    {
        this.maxEnemySize = maxEnemySize;
    }

    //Getters
    public float GetMinEnemySize()
    {
        return minEnemySize;

    }
    public float GetMaxEnemySize()
    {
        return maxEnemySize;

    }
}

