﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : Singleton<PoolManager> {


    public int initialPoolSize = 8;
    Dictionary<int, List<PoolableObject>> objectPool = new Dictionary<int, List<PoolableObject>>();

    #region Optimized

    Dictionary<int, int> indexPool = new Dictionary<int, int>();

    public void CreatePool(PoolableObject prefab, int poolSize)
    {
        int ID = prefab.GetInstanceID();
        objectPool[ID] = new List<PoolableObject>(poolSize);
        indexPool[ID] = 0;//Adding Key to dictionary with value 0
        for (int i = 0; i < poolSize; ++i)
        {
            addObject(ID, prefab);
        }
        print("Created a Pool for: " + ID);
    }

    public PoolableObject GetObjectFromPool(PoolableObject prefab)
    {
        int ID = prefab.GetInstanceID();

        //If i dont have a pool for this object then create one
        if (!objectPool.ContainsKey(ID))
            CreatePool(prefab, initialPoolSize);

        //Find next disabled object
        for (int i = 0; i < objectPool[ID].Count; ++i)
        {
            ++indexPool[ID];
            if (indexPool[ID] >= objectPool[ID].Count)
                indexPool[ID] = 0;

            if (!objectPool[ID][indexPool[ID]].gameObject.activeInHierarchy)
            {
                return objectPool[ID][indexPool[ID]];
            }
        }
        return addObject(ID, prefab);
    }


    #endregion

    private PoolableObject addObject(int ID, PoolableObject prefab)
    {
        PoolableObject clone = Instantiate<PoolableObject>(prefab);
        clone.gameObject.SetActive(false);
        objectPool[ID].Add(clone);
        return clone;
    }


}
