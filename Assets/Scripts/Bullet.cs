﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : PoolableObject{

    public float speed=20;
    private float enemySizeIncrease = 0.1f;
  
    public float smallSizeRef=0.5f;
    public float mediumSizeRef = 0.7f;
    public float maxSizeRef = 1;
    public float smallDamageRef = 0.5f;
    public float mediumDamageRef = 0.75f;
    public float maxDamageRef = 1;
    public float smallEnemyIncrease=0.1f;
    public float mediumEnemyIncrease = 0.15f;
    public float maxEnemyIncrease = 0.2f;

    private float currentPower;
    private float damageMod = 1;//Normal damage = 1/ Double Damage = 2 ...

    int colorID =0;
    private float size=1;
    private SpriteRenderer spriteR;
    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteR = GetComponent<SpriteRenderer>();
    }

    
    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        var enemyScript = otherCollider.GetComponent<Enemy>();
        if (enemyScript != null)
        {
            //print("HIT");
            if (colorID == enemyScript.GetColorID())
            {
                enemyScript.ChangeHealth(-(currentPower*damageMod-enemyScript.GetResistance()));//Apply damage with all its modifiers
            }
            else
            {
              //  print("WrongColor");
                enemyScript.SetSize(enemyScript.GetSize()+ enemySizeIncrease); //Gets the actual size and adds more to it.
            }
            gameObject.SetActive(false);
        } else if (otherCollider.GetComponent<MapEdge>() != null)
        {
         //   print("EDGE");
            gameObject.SetActive(false);
        }
      
      
    }

    public void SetValues(int colorNum, Color color, Vector3 position, Quaternion rotation, float bulletSize)//After being instantiated this method will be called to set its variables
    {
        //Change color if its different
        if (colorID != colorNum) {
            colorID = colorNum;
            spriteR.color = color;
        }
        //Set size and then set the scale according to its size
        size = bulletSize;
        transform.localScale= new Vector3(size,size);
        AssignValuesWithSize();
        //Set position and rotation
        transform.position = position;
        transform.rotation = rotation;
    }
    protected override void OnEnable_PO()
    {
        rb.velocity = transform.up * speed;

    }

    protected override void OnDisable_PO()
    {

    }

    private void AssignValuesWithSize()
    {
        if (size < mediumSizeRef)
        {
            currentPower = smallDamageRef;
            enemySizeIncrease = smallEnemyIncrease;
        }
        else if (size < maxSizeRef)
        {
            currentPower = mediumDamageRef;
            enemySizeIncrease = mediumEnemyIncrease;
        }
        else
        {
            currentPower = maxDamageRef;
            enemySizeIncrease = maxEnemyIncrease;
        }


    }

}
